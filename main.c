/*
 * Ghettolang:
 * at first named because of racism
 * then it changed to the programming style, aka,
 * a LOT of hacks and a general clusterfuck
 */

#include "ghettolang.h"

int main(int argc, char** argv) {
    do_next = 0;
    curline = 1;
    for (int i = 0; i < MAX_VARS; i++) data[i].used = 0;
    if (argc != 1) throw_error(1, "Usage: ghettolang < file.gl", curline);
    for (char* func = engets(stdin); func != NULL; func = engets(stdin)) {
        if (if_statement) {
            if (do_next) interpreter(func, curline);
            if_statement = 0;
            do_next = 0;
        } else {
            interpreter(func, curline);
        }
        curline++;
    }

    return 0;
}

