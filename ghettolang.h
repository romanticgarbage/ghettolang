#include <stdio.h>

#define MAX_VARS 4096
#define MAX_ARGS 4096

char *args[MAX_ARGS];

typedef struct {
    int used;
    int type;
    char *name;
    char *data_str;
    int data_int;
} data_t;
data_t data[MAX_VARS];

int maxvars;
int curline;
int if_statement, do_next;

char *engets(FILE *stream);

void interpreter(char *func, int curline);

void free_args(int range);

int check_in_data(char *needle);

void throw_error(int type, char *err, int curline);
