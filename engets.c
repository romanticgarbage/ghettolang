#include <stdlib.h>

#include "ghettolang.h"

char *engets(FILE *stream) {
    char *str_ptr = malloc(1 * sizeof(char)), *tmp_ptr;
    int i, c, len = 1;

    for (i = 0; ; i++) {
        if ((c = fgetc(stream)) == '\n') {
            str_ptr[i] = '\0';
            return str_ptr;
        } else if (c == EOF) {
            free(str_ptr);
            return NULL;
        }
        str_ptr[i] = (char)c;
        if ((tmp_ptr = realloc(str_ptr, ++len)) == NULL) {
            free(str_ptr);
            return NULL;
        } else {
            str_ptr = tmp_ptr;
        }
    }
}
