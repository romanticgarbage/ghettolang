#include <stdlib.h>
#include <string.h>

#include "ghettolang.h"

void free_args(int range) {
    for (int i = 0; i < range; i++)
        free(args[i]);
    return;
}

int check_in_data(char *needle) {
    for (int i = 0; i < maxvars; i++)
        if (!strcmp(data[i].name, needle))
            if (data[i].used) return i;
            else return -1;

    return -1;
}
