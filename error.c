/* Types:
 * 0: warning
 * 1: error */

#include <stdio.h>
#include <stdlib.h>

#include "ghettolang.h"

void throw_error(int type, char* err, int curline) {
    if (!type) fprintf(stderr, "WARNING (line %d): ", curline);
    else fprintf(stderr, "ERROR (line %d): ", curline);
    fputs(err, stderr);
    putchar('\n');
    if (type) exit(1);
    else return;
}
