#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ghettolang.h"

void interpreter(char* func, int curline) {
    int argsctr = 0;

    for (char *tok = strtok(func, " "); tok != NULL; tok = strtok(NULL, " ")) { /* temporary */
        args[argsctr] = malloc(strlen(tok) + 5);
        strcpy(args[argsctr], tok);
        argsctr++;
    }

    if (!strcmp("yell", args[0])) {
        if (argsctr == 1) throw_error(1, "yell without any arguments", curline);
        for (int i = 1; i < argsctr; i++) {
            fputs(args[i], stdout);
            putchar(' ');
        }
        putchar('\n');
    } else if (!strcmp("str", args[0])) {
        if (argsctr == 1) throw_error(1, "str without any arguments", curline);
        int added = 0;
        for (int i = 0; i <= maxvars + 1; i++) {
            if (!data[i].used) {
                data[i].used = 1;
                data[i].type = 1;
                data[i].name = malloc(strlen(args[1]) + 5);
                data[i].data_str = malloc(strlen(args[2]) + 5);
    
                strcpy(data[i].name, args[1]);
                if (argsctr == 3) strcpy(data[i].data_str, args[2]);
                else if (argsctr == 2) strcpy(data[i].data_str, "");
    
                if (i == maxvars) maxvars++;
                added++;
                break;
            }
        }
        if (!added) throw_error(1, "Could not create a string", curline);
    } else if (!strcmp("print_data", args[0])) {
        if (argsctr == 1) throw_error(1, "print_data without any arguments", curline);
        int check = check_in_data(args[1]);
        if (check == -1) throw_error(1, "Data with that name doesn't exist", curline);
        else
            if (!data[check].type) fprintf(stdout, "%d\n", data[check].data_int);
            else fprintf(stdout, "%s\n", data[check].data_str);
    } else if (!strcmp("int", args[0])) {
        if (argsctr == 1) throw_error(1, "int without any arguments", curline);
        int added = 0;
        for (int i = 0; i <= maxvars + 1; i++) {
            if (!data[i].used) {
                data[i].used = 1;
                data[i].type = 0;
                data[i].name = malloc(strlen(args[1]) + 5);

                strcpy(data[i].name, args[1]);
                if (argsctr == 3) data[i].data_int = atoi(args[2]);
                else if (argsctr == 2) data[i].data_int = 0;
                else throw_error(1, "Syntax error", curline);

                if (i == maxvars) maxvars++;
                added++;
                break;
            }
        }
        if (!added) throw_error(1, "Could not add an integer", curline);
    } else if (!strcmp("free", args[0])) {
        if (argsctr == 1) throw_error(1, "free without any arguments", curline);
        int check = check_in_data(args[1]);
        if (check == -1) throw_error(1, "Trying to free nonexistent data", curline);
        else data[check].used = 0;
    } else if (!strcmp("if", args[0])) {
        if_statement = 1;
        if (argsctr != 4) throw_error(1, "Syntax error", curline);
        int check1 = check_in_data(args[1]);
        int check2 = check_in_data(args[3]);
        if (check1 == -1) check1 = atoi(args[1]);
        else
            if (!data[check1].type) check1 = data[check1].data_int;
            else check1 = atoi(data[check1].data_str);
        if (check2 == -1) check2 = atoi(args[3]);
        else
            if (!data[check1].type) check2 = data[check2].data_int;
            else check2 = atoi(data[check2].data_str);

        if (!strcmp("==", args[2]))
            if (check1 == check2) do_next = 1;
        else if (!strcmp("!=", args[2]))
            if (check1 != check2) do_next = 1;
        else if (!strcmp(">", args[2]))
            if (check1 > check2) do_next = 1;
        else if (!strcmp("<", args[2]))
            if (check1 < check2) do_next = 1;
        else if (!strcmp(">=", args[2]))
            if (check1 >= check2) do_next = 1;
        else if (!strcmp("<=", args[2]))
            if (check1 <= check2) do_next = 1;
        else
            throw_error(1, "Unknown operator", curline);
    } else {
        int check = check_in_data(args[0]);
        if (check == -1) throw_error(1, "Unknown function used", curline);
        if (!strcmp("=", args[1])) {
            if (data[check].type) throw_error(1, "Cannot assign value to a string", curline);

            if (argsctr == 3) data[check].data_int = atoi(args[2]);
            else if (argsctr == 5) {
                if (!strcmp("+", args[3])) data[check].data_int = atoi(args[2]) + atoi(args[4]);
                else if (!strcmp("-", args[3])) data[check].data_int = atoi(args[2]) - atoi(args[4]);
                else if (!strcmp("/", args[3])) data[check].data_int = atoi(args[2]) / atoi(args[4]);
                else if (!strcmp("*", args[3])) data[check].data_int = atoi(args[2]) * atoi(args[4]);
                else throw_error(1, "Unknown math operator used", curline);
            }
        }

    }

    free_args(argsctr);
    return;
}
